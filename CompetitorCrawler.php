<?php
	require_once('keysMuthu.php');
	require_once('eBaySession.php');
	require_once('DBConnect.php');
	require_once('email.php');
	
	ob_implicit_flush(true);
	$mysqli = openDBconnect();
	$dbmysqli = DBconnect();
	$objmail = new objmail();

	$updqry = " UPDATE competitor SET LastRunFromDate = NULL, LastRunToDate = NULL, LastPageNo = NULL, TotalPage = NULL WHERE ProcessFlag = 1";
	$mysqli->query($updqry);

	$cbrinstance = 'Romaine';
	$result = $resultItem = array();
	$qry = " SELECT compid, competitorname, firstlistingdate AS inceptiondate, lastrunfromdate, lastruntodate, lastpageno, totalpage, lastrunid, errortype FROM competitor WHERE processflag = 1
			AND IFNULL(lastruntodate, NOW()) <= NOW() ORDER BY 3 DESC, compid ";

	// $qry = " SELECT compid, competitorname, inceptiondate, lastrunfromdate, lastruntodate, lastpageno, totalpage, lastrunid, errortype FROM competitor WHERE processflag = 1 ";
	 
	$res = $mysqli->query($qry);
	while($row = $res->fetch_assoc()) 
	{
		$result[] = $row;
	}
	//echo "<pre>"; print_r($result);
	//exit();
	$filter = 0;
	$br = "";
	//$categoryid = 178921;
 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<TITLE>Competitor Crawler</TITLE>
</HEAD>
<BODY>

<?php
 	$recordDate = $startDate = date('Y-m-d H:i:s');
	$accountid = 'PG001';

	$objmail->mailfunction(array("subject" => 'Romaine - DIVE CBR Data Processing Started', "sendmail" => "Yes", "CC" => "CC: vaithi.d@apaengineering.com", "msg" => 'This is a automated mailer to intimate that the Romaine CBR Data processing program started succesfully.'));

	$insrtqry = " INSERT INTO pg_schedule_tracker_header (StartDate, AccountID, RecordDate) VALUES 
				('".$startDate."', '".$accountid."', '".$recordDate."') ";
	$dbmysqli->query($insrtqry);
	$last_id = $dbmysqli->insert_id;
	
	$spbefreqry = "CALL pg_before_cron_run_process();";
	$mysqli->query($spbefreqry);
	
	//echo "=====".$last_id; exit();
	$total = count($result);
	$balncecomp = 1;
	
	if(count($result) > 0)
	{
		foreach($result as $reset)
		{
			$compid = $reset['compid'];
			$competitorname = $reset['competitorname'];
			$id = $competitorname;
			$inceptiondate = $reset['inceptiondate'];
			$lastrunfromdate = $reset['lastrunfromdate'];
			$lastruntodate = $reset['lastruntodate'];
			$lastpageno = $reset['lastpageno'];
			$totalpage = $reset['totalpage'];
			$lastrunid = $reset['lastrunid'];
			$errortype = $reset['errortype'];
			
			$interval = '30 days';

			$listingyear = 1990;
			
			$ddate = date('Y-m-d');
			$date = new DateTime($ddate);
			$week = $date->format("W");
			
			$today = date("l");
			if ($today == 'Sunday') {
				$weekno = ($week + 1);
			} else {
				$weekno = $week;
			}
	
			echo "\r\n";
			echo "Processing Competitor : For " . $cbrinstance . " : " . $competitorname;
			echo "\r\n";

			$format = 'Y-m-d H:i:s';
			$lasttodate = DateTime::createFromFormat($format, '1980-01-01 01:01:01');
			$lastdate = date_format($lasttodate, 'Y-m-d');
			$runonce = 1; 
			$schdulerID = $last_id;  $recordDate = $startDate = date('Y-m-d H:i:s');
			
			$insqry = " INSERT INTO pg_schedule_tracker_detail (SchedulerID, AccountID, CompetitorID, StartDate, CronStatus, RecordDate) VALUES 
			          ('".$schdulerID."', '".$accountid."', '".$id."', '".$startDate."', 'InProgress', '".$recordDate."') ";
			$dbmysqli->query($insqry);	  
			$autoid = $dbmysqli->insert_id;

			$remaineComp = $total - $balncecomp;
			$trackqry = "call dive_api_run_track('CBR Process Started', 'CBR PG', '".$id."', 'NA', ".$total.", ".$remaineComp.", now(), week(now()))";
			$mysqli->query($trackqry);
			
			$mailersub = 'Romaine - DIVE CBR Data Processing Started - ' . $id;
			
			$objmail->mailfunction(array("subject" => $mailersub, "sendmail" => "No", "CC" => "CC: vaithi.d@apaengineering.com", "msg" => 'This is a automated mailer to intimate that the Romaine CBR Data processing program started succesfully.'));
			
			$runid = 0;
			$currentyear = date("Y");
			while ($lastdate < date("Y-m-d") and substr($lastdate, 0, 4) <= $currentyear and substr($lastdate, 5, 2) <= 12 and $runonce == 1)
			{
				$lastrunid = $lastrunid + 1;
				$runonce = 1;

				if (is_null($inceptiondate))
				{
					$todate = date("Y-m-d");
					$d=strtotime("-1 day");
					$fromdate = date("Y-m-d",$d);	

					$ModTimeFrom = $fromdate.'T00:00:00';
					$ModTimeTo = $todate.'T00:00:00';
						
					$siteID = 0;
					$verb = 'GetSellerList';

					///Build the request Xml string
					$requestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
					$requestXmlBody .= '<GetSellerListRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
					$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$userToken</eBayAuthToken></RequesterCredentials>";
					$requestXmlBody .= "<Pagination>";
					$requestXmlBody .= "<EntriesPerPage>200</EntriesPerPage>";
					$requestXmlBody .= "<PageNumber>1</PageNumber>";
					$requestXmlBody .= " </Pagination>";
					if ($filter == 1)
					{
						$requestXmlBody .= "<CategoryID>$categoryid</CategoryID>";	
					}
					$requestXmlBody .= "<StartTimeFrom>$ModTimeFrom</StartTimeFrom>";
					$requestXmlBody .= "<StartTimeTo>$ModTimeTo</StartTimeTo>";
					$requestXmlBody .= "<UserID>$id</UserID>";
					$requestXmlBody .= "<GranularityLevel>Fine</GranularityLevel>";
					$requestXmlBody .= '</GetSellerListRequest>';
					//Create a new eBay session with all details pulled in from included keys.php
					$session = new eBaySession($userToken, $devID, $appID, $certID, $serverUrl, $compatabilityLevel, $siteID, $verb);

					//send the request and get response
					$responseXml = $session->sendHttpRequest($requestXmlBody);
					$responseDoc = new DomDocument();
					$responseDoc->loadXML($responseXml);
					
					$xml = simplexml_load_string($responseXml);

					echo "<pre>";
					//print_r($xml);
					//exit();
					if (stristr($responseXml, 'exceeded usage limit') || $responseXml == '')
					{
						$pageno = $totpages = 0; 
						$updateCompQry = "update competitor set LastRunFromDate = '" . $lastrunfromdate . "', LastRunToDate = '" . $lastruntodate . "', LastPageNo = " . $pageno . ",
						TotalPage = " . $totpages . ", LastRunID = " . $lastrunid . ", errortype = '" . $responseXml . "', recorddate = now() where compid = " . $compid;
					//echo "===".$updateCompQry; exit();
						if (!$mysqli->query($updateCompQry)) {
								
//------------MAIL FUNCTION---------------
							$objmail->mailfunction(array("subject" => 'Romaine CBR - exceeded usage limit', "sendmail" => "No", "CC" => "CC: vaithi.d@apaengineering.com",  
							"msg" => $responseXml));

							die('<P>Error thrown : ' .  $mysqli->error);
						} else {
							$mysqli->query($updateCompQry);
						}
						
						$errormsg = '<P>Error sending request' . $responseXml;
						echo "error=====".$responseXml;
						die($errormsg);
					}

					//Xml string is parsed and creates a DOM Document object
					$responseDoc = new DomDocument();
					$responseDoc->loadXML($responseXml);
					
					$xml = simplexml_load_string($responseXml);
					//print_r($xml);
					
					$inceptiondate = $xml->Seller->RegistrationDate;
					$inceptiondate = str_replace("T"," ",$inceptiondate);
					$inceptiondate = str_replace(".000Z","",$inceptiondate);
					
					$sellerStatus = $xml->Seller->Status;
					$sellerSite = $xml->Seller->Site;
					$sellerLevel = $xml->Seller->SellerInfo->SellerLevel;
					
					$insertQry = "update competitor set InceptionDate = '" . $inceptiondate . "', CompetitorStatus = '".$sellerStatus."', CompetitorSite = '".$sellerSite."',
									  Competitorlevel = '".$sellerLevel."' where CompID = '" . $compid . "'";
					if (!$mysqli->query($insertQry)) {
						die('<P>Error thrown : ' .  $mysqli->error);
					} else {
						$mysqli->query($insertQry);
					}
					
					unset($session);
				}

				if (is_null($lastpageno) and is_null($totalpage))
				{
					$format = 'Y-m-d H:i:s';
					$todate = DateTime::createFromFormat($format, $inceptiondate);
					$lastrunfromdate = date_format($todate, 'Y-m-d H:i:s');
					$ModTimeFromDt = date_format($todate, 'Y-m-d');
					date_add($todate, date_interval_create_from_date_string($interval));
					
					if (date_format($todate, 'Y-m-d') > date('Y-m-d')) 
					{
						$todate = DateTime::createFromFormat($format, date('Y-m-d'));
					}

					$lastruntodate = date_format($todate, 'Y-m-d H:i:s');
					$ModTimeToDt = date_format($todate, 'Y-m-d');
					$lastdate = date_format($todate, 'Y-m-d');			
					$lastpageno = 1;
					$totalpage = 1;
					$pageno = 1;
				} 
				else if ($lastpageno < $totalpage)
				{
					$format = 'Y-m-d H:i:s';
					$fromdate = DateTime::createFromFormat($format, $lastrunfromdate);
					$lastrunfromdate = date_format($fromdate, 'Y-m-d H:i:s');
					$ModTimeFromDt = date_format($fromdate, 'Y-m-d');
					$todate = DateTime::createFromFormat($format, $lastruntodate);
					$lastruntodate = date_format($todate, 'Y-m-d H:i:s');
					$ModTimeToDt = date_format($todate, 'Y-m-d');
					$lastdate = date_format($fromdate, 'Y-m-d');			
					$pageno = $lastpageno;
					$totpages = $totalpage;
				}
				else
				{
					$format = 'Y-m-d H:i:s';
					$fromdate = DateTime::createFromFormat($format, $lastruntodate);
					$lastrunfromdate = date_format($fromdate, 'Y-m-d H:i:s');
					$ModTimeFromDt = date_format($fromdate, 'Y-m-d');
					date_add($fromdate, date_interval_create_from_date_string($interval));
					
					if (date_format($fromdate, 'Y-m-d') > date('Y-m-d')) 
					{
						$todate = DateTime::createFromFormat($format, date('Y-m-d'));
					}

					$lastdate = date_format($fromdate, 'Y-m-d');			
					$lastruntodate = date_format($fromdate, 'Y-m-d H:i:s');
					$ModTimeToDt = date_format($fromdate, 'Y-m-d');
					$lastpageno = 1;
					$totalpage = 1;
					$pageno = 1;
				}

				$ModTimeFrom = $ModTimeFromDt . 'T00:00:00';
				$ModTimeTo = $ModTimeToDt . 'T00:00:00';
				
				echo "For " . $cbrinstance . " : Seller - " . $id;
				echo "\r\n";
				echo 'Mode From Time :' . $ModTimeFrom .  $br;
				echo "\r\n";
				echo 'Mode To Time :' . $ModTimeTo . $br;
				echo "\r\n";

				$siteID = 0;
				$verb = 'GetSellerList';

				///Build the request Xml string
				$requestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
				$requestXmlBody .= '<GetSellerListRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
				$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$userToken</eBayAuthToken></RequesterCredentials>";
				$requestXmlBody .= "<Pagination>";
				$requestXmlBody .= "<EntriesPerPage>200</EntriesPerPage>";
				$requestXmlBody .= "<PageNumber>$pageno</PageNumber>";
				$requestXmlBody .= "</Pagination>";
				$requestXmlBody .= "<StartTimeFrom>$ModTimeFrom</StartTimeFrom>";
				$requestXmlBody .= "<StartTimeTo>$ModTimeTo</StartTimeTo>";
				if ($filter == 1)
				{					
					$requestXmlBody .= "<CategoryID>$categoryid</CategoryID>";
				}
				$requestXmlBody .= "<UserID>$id</UserID>";
				$requestXmlBody .= "<GranularityLevel>Fine</GranularityLevel>";
				$requestXmlBody .= "<IncludeVariations>true</IncludeVariations>";
				$requestXmlBody .= '</GetSellerListRequest>';
				
				//echo "<pre>"; print_r($requestXmlBody);
				//exit;
				
				//Create a new eBay session with all details pulled in from included keys.php
				$session = new eBaySession($userToken, $devID, $appID, $certID, $serverUrl, $compatabilityLevel, $siteID, $verb);

				//send the request and get response
				$responseXml = $session->sendHttpRequest($requestXmlBody);
				
				if(isset($responseXml->Errors))
						{
							$errorsset = $xml->Errors->ErrorCode;	
							
							if ($errorsset == 518)
							{
								die('<P>Exceeded Daily Call Limit');
							}
							else
							{
								die('<P>Exceeded Daily Call Limit ' . $errorsset);
							}
						}
						

				if(isset($responseXml->Errors))
				{
					//print_r($responseXml);
					$errorsset = $xml->Errors->ErrorCode;
					if ($errorsset == 518)
					{
						$updateCompQry = "update competitor set LastRunFromDate = '" . $lastrunfromdate . "', LastRunToDate = '" . $lastruntodate . "', LastPageNo = " . $pageno . ",
						TotalPage = 0, LastRunID = " . $lastrunid . ", errortype = '" . $responseXml . "', recorddate = now() where compid = " . $compid;
						if (!$mysqli->query($updateCompQry)) {
	//--------------------MAIL FUNCTION---------------			
								$objmail->mailfunction(array("subject" => 'Romaine CBR - exceeded usage limit', "sendmail" => "No", "CC" => "CC: vaithi.d@apaengineering.com", 
								"msg" => $responseXml));							
							die('<P>Error thrown : ' .  $mysqli->error);
						} else {
							$mysqli->query($updateCompQry);
						}
						die('<P>Exceeded Daily Call Limit');
					}
					else
					{
						die('<P>Exceeded Daily Call Limit ' . $errorsset);
					}
				}
					
				//Xml string is parsed and creates a DOM Document object
				$responseDoc = new DomDocument();
				$responseDoc->loadXML($responseXml);
				
				$xml = simplexml_load_string($responseXml);
				// print_r($xml);
				
				if($runid == 0) {
					if(isset($xml->Seller)) {
						$sellerStatus = $xml->Seller->Status;
						$sellerSite = $xml->Seller->Site;
						$sellerLevel = $xml->Seller->SellerInfo->SellerLevel;

						$updQry = "update competitor set CompetitorStatus = '".$sellerStatus."', CompetitorSite = '".$sellerSite."',
									  Competitorlevel = '".$sellerLevel."' where CompID = '" . $compid . "'";

						if (!$mysqli->query($updQry)) {
							die('<P>Error thrown : ' .  $mysqli->error);
						} else {
							$mysqli->query($updQry);
						}
					}
				}
				
				$totrec = $totalpages = 0;
				$totpages = $xml->PaginationResult->TotalNumberOfPages;
				$totrecds = $xml->PaginationResult->TotalNumberOfEntries;
				
				if ($totrecds == '' || $totpages == '') {
					$totrec = 0;
					$totalpages = 0;
				} else {
					$totrec = $totrecds;
					$totalpages = $totpages;
				}
				
				if ($totrecds == 0)
				{
					//echo "<p style='color:red'>Total Number of Listings :" . $totrec . "</p><br>";
					echo "Total Number of Listings :" . $totrec;
					echo "\r\n";
					echo "Total Number of Pages :" . $totpages .  $br;
					echo "\r\n";
				}
				else
				{
					// echo "<p style='color:blue'>Total Number of Listings :" . $totrec . "</p><br>";
					echo "Total Number of Listings :" . $totrec;
					echo "\r\n";
					echo "Total Number of Pages :" . $totpages .  $br;	
					echo "\r\n";
				}
				// echo "<hr>";
				echo "\r\n";

				$SumQry = "INSERT into competitor_run_status(CompID, ListingFrom, ListingTo, RunID, Listings, PageNo, TotalPage, RecordDate) values(" . $compid . ",
				'" . $lastrunfromdate . "', '" . $lastruntodate . "', '" . $lastrunid . "',  '" . $totrec . "', '".$pageno."', '".$totalpages."', now())";
			//	echo "inserttqry ======".$SumQry; exit();
				
				if (!$mysqli->query($SumQry))
				{
					echo $SumQry;
					$updateqry = " UPDATE pg_schedule_tracker_detail SET Error = '".$mysqli->error."', CronStatus = 'Error' 
									WHERE CompetitorID = '".$compid."' ";
					$dbmysqli->query($updateqry);
//------------MAIL FUNCTION---------------			
					$objmail->mailfunction(array("subject" => 'Romaine CBR - Run Status Error', "sendmail" => "No", "CC" => "CC: vaithi.d@apaengineering.com", "msg" => $mysqli->error));		
					die('<P>Error thrown : ' .  $mysqli->error);
				} else {
					$mysqli->query($SumQry);
				}
				
				unset($session);
				
				foreach($xml->ItemArray->Item as $resultval) 
				{
					$startdate = $resultval->ListingDetails->StartTime;
					$datetm = str_replace("T"," ",$startdate);
					$datetm = str_replace(".000Z","",$datetm);
					$listedon = DateTime::createFromFormat('Y-m-d H:i:s',$datetm);
					$listingdate = $listedon->format('Y-m-d H:m:s');
					$listingyear = $listedon->format('Y');
					$listingweek = $listedon->format('W');

					$itemID = $resultval->ItemID;
					$sku = $resultval->SKU;
					$title = $resultval->Title;
					$category = $resultval->PrimaryCategory->CategoryName;
					$categoryID = $resultval->PrimaryCategory->CategoryID;
					$quantity = $resultval->Quantity;
					$currprice = $resultval->SellingStatus->CurrentPrice;
					$qtysold = $resultval->SellingStatus->QuantitySold;
					$ListingStatus = $resultval->SellingStatus->ListingStatus;
					//$ViewItemURL = $resultval->ListingDetails->ViewItemURL;
					$GalleryURL = $resultval->PictureDetails->GalleryURL;
					$ViewItemURL = 'http://www.ebay.com/itm/'.$itemID;
					
					$shipping = ''; $servicecost = 0;
					$itemcountry = $resultval->Country;
					$itemLocation = $resultval->Location;
					$ConditionID = $resultval->ConditionID;
					$ConditionDisplayName = $resultval->ConditionDisplayName;
					$ShippingService = $resultval->ShippingDetails->ShippingServiceOptions;
					
					if (count($ShippingService) > 0) {
						$servicecostval  = $ShippingService->ShippingServiceCost;
						$shipping = $ShippingService->FreeShipping;
					} else {
						$servicecostval = $ShippingService->ShippingServiceCost;
					}
					
					if ($servicecostval == '') {
						$servicecost = 0;
					} else {
						$servicecost = $servicecostval;
					}
					
					if ($servicecost > 0) {
						$shipping = 'false';
					} else {
						$shipping = 'True';
					}
					
					$variationcount = 0;

					/* $variationcount = count($resultval->Variations->Variation);

					if (!is_null($resultval->Variations->Variation)) 
					{
						$variationcount = count($resultval->Variations->Variation);

						foreach ($resultval->Variations->Variation as $variantlist)
						{
							if ($variantlist->VariationSpecifics->NameValueList->Name = 'Model') 
							{
								$model = $variantlist->VariationSpecifics->NameValueList->Value;
							}

							$variantsku = $variantlist->SKU;
							
							$variantsku = str_replace("'", "\'" ,$variantsku);
							$variantsku = str_replace('"', '\"' ,$variantsku);
					
							$variantPrice = $variantlist->StartPrice;
							$vavriantQuantitySold = $variantlist->SellingStatus->QuantitySold;

							$insertQry2 = "INSERT into competitor_item_variant_rawlist(CompID, RunID, ItemID, SKU, Model, Price, QuantitySold, RecordDate, WeekNo, YearNo, Created_by, Created_On) values(" . $compid . ", " . $lastrunid . ", '". $itemID . "', '" . $variantsku . "', '" . $model . "', " . $variantPrice . ", " . $vavriantQuantitySold . ", now(), WEEK(NOW()), YEAR(NOW()), 'DIVE', now())";
							
							if (!$mysqli->query($insertQry2)) {
								echo $insertQry2;
								$updteqry = " UPDATE pg_schedule_tracker_detail SET Error = '".$mysqli->error."', CronStatus = 'Error' 
											 WHERE CompetitorID = '".$compid."' ";
								$dbmysqli->query($updteqry);	
		//------------MAIL FUNCTION---------------
								$objmail->mailfunction(array("subject" => 'Competitor Items Insert Error', "sendmail" => "No", "CC" => "CC: vaithi.d@apaengineering.com", "msg" => $mysqli->error));
								die('<P>Error thrown : ' .  $mysqli->error);
							} else {
								$mysqli->query($insertQry2);
							}
						}
					}			 */		

					$TopRatedListing = '';
					$sku = str_replace("\\", "/" ,$sku);
					$sku = str_replace("'", "\'" ,$sku);
					$sku = str_replace('"', '\"' ,$sku);
					
					$category = str_replace("\\", "/" ,$category);
					$category = str_replace("'", "\'" ,$category);
					$category = str_replace('"', '\"' ,$category);
					
					$itemLocation = str_replace("\\", "/" ,$itemLocation);
					$itemLocation = str_replace("'", "\'" ,$itemLocation);
					$itemLocation = str_replace('"', '\"' ,$itemLocation);
					
					$title = str_replace("\\", "/" ,$title);
					$title = str_replace("'", "\'" ,$title);
					$title = str_replace('"', '\"' ,$title);
					
					$insertQry = "INSERT into competitor_item_details(CompID, ItemID, SKU, Title, Category, CategoryID, Quantity, CurrentPrice, QuantitySold, ListingStatus, ItemURL, ListingDate, RunID, RecordDate, WeekNo, YearNo, Country, Location, ConditionID, ConditionName, ShippingCost, FreeShipping, VariantCount, TopRatedListing)
					values(" . $compid . ", '" . $itemID . "', '" . $sku . "', '" . $title . "', '" . $category . "', '" .  $categoryID . "', '" .$quantity . "', '" . $currprice . "',
					'" . $qtysold . "', '" . $ListingStatus . "', '" . $ViewItemURL. "', '" . $listingdate . "', " . $lastrunid . ", now(), ".$weekno.", YEAR(NOW()), '".$itemcountry."',
					'".$itemLocation."', '".$ConditionID."', '".$ConditionDisplayName."', '".$servicecost."', '".$shipping."', " . $variationcount . " , '" . $TopRatedListing . "' )";
					
					if (!$mysqli->query($insertQry)) {
						echo $insertQry;
						$updteqry = " UPDATE pg_schedule_tracker_detail SET Error = '".$mysqli->error."', CronStatus = 'Error' 
									 WHERE CompetitorID = '".$compid."' ";
						$dbmysqli->query($updteqry);	
//------------MAIL FUNCTION---------------
						$objmail->mailfunction(array("subject" => 'Romaine CBR - Competitor Items Insert Error', "sendmail" => "No", "CC" => "CC: vaithi.d@apaengineering.com", 
						"msg" => $mysqli->error));
						die('<P>Error thrown : ' .  $mysqli->error);
					} else {
						$mysqli->query($insertQry);
					}
					//$mysqli->query($insertQry);
							$insertPicQry = "INSERT into competitor_item_picture_list(CompID, ItemID, RunID, PictureID, PictureURL, WeekNo, YearNo, Created_By, Created_On) 
											values(" .$compid. ", " .$itemID. ", '".$lastrunid. "', 0, '" .$GalleryURL. "', WEEKOFYEAR(NOW()), YEAR(NOW()), 'DIVE-CBR', now())";	
											
							if (!$mysqli->query($insertPicQry)) {
								$updteqry = " UPDATE pg_schedule_tracker_detail SET Error = '".$mysqli->error."', CronStatus = 'Error' 
									 WHERE CompetitorID = '".$compid."' ";
								$dbmysqli->query($updteqry);	
//------------MAIL FUNCTION---------------
								$objmail->mailfunction(array("subject" => 'Romaine CBR - Competitor Items Insert Error', "sendmail" => "No", "CC" => "CC: vaithi.d@apaengineering.com", 
								"msg" => $mysqli->error));
								die('<P>Error thrown : ' .  $mysqli->error);
							} else {
								$mysqli->query($insertPicQry);
							}
				}

				$updateCompQry = "update competitor set LastRunFromDate = '" . $lastrunfromdate . "', LastRunToDate = '" . $lastruntodate . "', LastPageNo = " . $pageno . ", 
				TotalPage = 0, LastRunID = " . $lastrunid . ", errortype = NULL, recorddate = now() where compid = " . $compid;
				if (!$mysqli->query($updateCompQry)) {
//------------MAIL FUNCTION---------------	
					$objmail->mailfunction(array("subject" => 'Romaine CBR - Competitor Error', "sendmail" => "No", "msg" => $mysqli->error));				
					die('<P>Error thrown : ' .  $mysqli->error);
				} else {
					$mysqli->query($updateCompQry);
				}
				//$mysqli->query($updateCompQry);

				if ($totpages > 1)
				{
					$lastpageno = $lastpageno + 1;
					echo "\r\n";

					for ($pageno = $lastpageno; $pageno <= $totpages; $pageno++)
					{

						echo $cbrinstance . " : Seller - " . $id . " : Process Page :" . $pageno;
						echo "\r\n";

						$requestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
						$requestXmlBody .= '<GetSellerListRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
						$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$userToken</eBayAuthToken></RequesterCredentials>";
						$requestXmlBody .= "<Pagination>";
						$requestXmlBody .= "<EntriesPerPage>200</EntriesPerPage>";
						$requestXmlBody .= "<PageNumber>$pageno</PageNumber>";
						$requestXmlBody .= " </Pagination>";
						if ($filter == 1)
						{					
							$requestXmlBody .= "<CategoryID>$categoryid</CategoryID>";
						}
						$requestXmlBody .= "<StartTimeFrom>$ModTimeFrom</StartTimeFrom>";
						$requestXmlBody .= "<StartTimeTo>$ModTimeTo</StartTimeTo>";
						$requestXmlBody .= "<UserID>$id</UserID>";
						$requestXmlBody .= "<GranularityLevel>Fine</GranularityLevel>";
						$requestXmlBody .= "<IncludeVariations>true</IncludeVariations>";
						$requestXmlBody .= '</GetSellerListRequest>';
						//Create a new eBay session with all details pulled in from included keys.php
						$session = new eBaySession($userToken, $devID, $appID, $certID, $serverUrl, $compatabilityLevel, $siteID, $verb);
						
						//send the request and get response
						$responseXml = $session->sendHttpRequest($requestXmlBody);

						if(isset($responseXml->Errors))
						{
							$errorsset = $xml->Errors->ErrorCode;
							if ($errorsset == 518)
							{
								$updateCompQry = "update competitor set LastRunFromDate = '" . $lastrunfromdate . "', LastRunToDate = '" . $lastruntodate . "', LastPageNo = " . $pageno . ", TotalPage = " . $totpages . ", LastRunID = " . $lastrunid . ", errortype = '" . $responseXml . "', recorddate = now() where compid = " . $compid;
								if (!$mysqli->query($updateCompQry)) {
	//--------------------MAIL FUNCTION---------------			
									$objmail->mailfunction(array("subject" => 'Romaine CBR - exceeded usage limit', "sendmail" => "No", "CC" => "CC: vaithi.d@apaengineering.com", "msg" => $responseXml));					
									die('<P>Error thrown : ' .  $mysqli->error);
								} else {
									$mysqli->query($updateCompQry);
								}
								//$mysqli->query($updateCompQry);
								
								die('<P>Exceeded Daily Call Limit');
							}
							else
							{
								die('<P>Exceeded Daily Call Limit ' . $errorsset);
							}
						}
						
						//Xml string is parsed and creates a DOM Document object
						$responseDoc = new DomDocument();
						$responseDoc->loadXML($responseXml);
						
						$xml2 = simplexml_load_string($responseXml);

						unset($session);
						
						foreach($xml2->ItemArray->Item as $resultval) 
						{
							$ErrorType = '';
							$startdate = $resultval->ListingDetails->StartTime;
							$datetm = str_replace("T"," ",$startdate);
							$datetm = str_replace(".000Z","",$datetm);
							$listedon = DateTime::createFromFormat('Y-m-d H:i:s',$datetm);
							$listingdate = $listedon->format('Y-m-d H:m:s');
							$listingyear = $listedon->format('Y');
							$listingweek = $listedon->format('W');

							$itemID = $resultval->ItemID;
							$sku = $resultval->SKU;
							$title = $resultval->Title;
							$category = $resultval->PrimaryCategory->CategoryName;
							$categoryID = $resultval->PrimaryCategory->CategoryID;
							$quantity = $resultval->Quantity;
							$currprice = $resultval->SellingStatus->CurrentPrice;
							$qtysold = $resultval->SellingStatus->QuantitySold;
							$ListingStatus = $resultval->SellingStatus->ListingStatus;
							//$ViewItemURL = $resultval->ListingDetails->ViewItemURL;
							$GalleryURL = $resultval->PictureDetails->GalleryURL;
							$ViewItemURL = 'http://www.ebay.com/itm/'.$itemID;
							
							$servicecost = 0; $shipping = '';
							$itemcountry = $resultval->Country;
							$itemLocation = $resultval->Location;
							$ConditionID = $resultval->ConditionID;
							$ConditionDisplayName = $resultval->ConditionDisplayName;
							$ShippingServicearr = $resultval->ShippingDetails->ShippingServiceOptions;
							
							if (count($ShippingServicearr) > 0) {
								$servicecostval  = $ShippingServicearr->ShippingServiceCost;
								$shipping = $ShippingServicearr->FreeShipping;
							} else {
								$servicecostval = $ShippingServicearr->ShippingServiceCost;
							}

							if ($servicecostval == '') {
								$servicecost = 0;
							} else {
								$servicecost = $servicecostval;
							}
					
							if ($servicecost > 0) {
								$shipping = 'false';
							} else {
								$shipping = 'True';
							}	
								
							$variationcount = 0;

							/* $variationcount = count($resultval->Variations->Variation);

							if (!is_null($resultval->Variations->Variation)) 
							{
								$variationcount = count($resultval->Variations->Variation);

								foreach ($resultval->Variations->Variation as $variantlist)
								{
									if ($variantlist->VariationSpecifics->NameValueList->Name = 'Model') 
									{
										$model = $variantlist->VariationSpecifics->NameValueList->Value;
									}

									$variantsku = $variantlist->SKU;
									
									$variantsku = str_replace("'", "\'" ,$variantsku);
									$variantsku = str_replace('"', '\"' ,$variantsku);
							
									$variantPrice = $variantlist->StartPrice;
									$vavriantQuantitySold = $variantlist->SellingStatus->QuantitySold;

									$insertQry2 = "INSERT into competitor_item_variant_rawlist(CompID, RunID, ItemID, SKU, Model, Price, QuantitySold, RecordDate, WeekNo, YearNo, Created_by, Created_On) values(" . $compid . ", " . $lastrunid . ", '". $itemID . "', '" . $variantsku . "', '" . $model . "', " . $variantPrice . ", " . $vavriantQuantitySold . ", now(), WEEK(NOW()), YEAR(NOW()), 'DIVE', now())";
									
									if (!$mysqli->query($insertQry2)) {
										echo $insertQry2;
										$updteqry = " UPDATE pg_schedule_tracker_detail SET Error = '".$mysqli->error."', CronStatus = 'Error' 
													 WHERE CompetitorID = '".$compid."' ";
										$dbmysqli->query($updteqry);	
				//------------MAIL FUNCTION---------------
										$objmail->mailfunction(array("subject" => 'Competitor Items Insert Error', "sendmail" => "No", "CC" => "CC: vaithi.d@apaengineering.com", "msg" => $mysqli->error));
										die('<P>Error thrown : ' .  $mysqli->error);
									} else {
										$mysqli->query($insertQry2);
									}
								}
							}	 */				

							$TopRatedListing = '';
							$sku = str_replace("\\", "/" ,$sku);
							$sku = str_replace("'", "\'" ,$sku);
							$sku = str_replace('"', '\"' ,$sku);
							
							$category = str_replace("\\", "/" ,$category);
							$category = str_replace("'", "\'" ,$category);
							$category = str_replace('"', '\"' ,$category);
							
							$itemLocation = str_replace("\\", "/" ,$itemLocation);
							$itemLocation = str_replace("'", "\'" ,$itemLocation);
							$itemLocation = str_replace('"', '\"' ,$itemLocation);
							
							$title = str_replace("\\", "/" ,$title);
							$title = str_replace("'", "\'" ,$title);
							$title = str_replace('"', '\"' ,$title);
							$insertQry = "INSERT into competitor_item_details(CompID, ItemID, SKU, Title, Category, CategoryID, Quantity, CurrentPrice, QuantitySold, ListingStatus, ItemURL, ListingDate, RunID, RecordDate, WeekNo, YearNo, Country, Location, ConditionID, ConditionName, ShippingCost, FreeShipping, VariantCount, TopRatedListing)
							values(" . $compid . ", '" . $itemID . "', '" . $sku . "', '" . $title . "', '" . $category . "', '" . $categoryID . "', '" .$quantity . "', '" . $currprice . "',
							'" . $qtysold . "', '" . $ListingStatus . "', '" . $ViewItemURL. "', '" . $listingdate . "', " . $lastrunid . ", now(), ".$weekno.", YEAR(NOW()), '".$itemcountry."', '".$itemLocation."', '".$ConditionID."', '".$ConditionDisplayName."', '".$servicecost."', '".$shipping."', " . $variationcount . " , '" . $TopRatedListing . "')";
							if (!$mysqli->query($insertQry)) {
								echo $insertQry;
								$upteqry = " UPDATE pg_schedule_tracker_detail SET Error = '".$mysqli->error."', CronStatus = 'Error' 
											WHERE CompetitorID = '".$compid."' ";
								$dbmysqli->query($upteqry);	
//--------------------MAIL FUNCTION---------------
								$objmail->mailfunction(array("subject" => 'Romaine CBR - Competitor Items Insert Error', "sendmail" => "No", "CC" => "CC: vaithi.d@apaengineering.com", 
								"msg" => $mysqli->error));
								die('<P>Error thrown : ' .  $mysqli->error);
							} else {
								$mysqli->query($insertQry);
							}
							//$mysqli->query($insertQry);
							$insertPicQry = "INSERT into competitor_item_picture_list(CompID, ItemID, RunID, PictureID, PictureURL, WeekNo, YearNo, Created_By, Created_On) 
											values(" .$compid. ", " .$itemID. ", '".$lastrunid. "', 0, '" .$GalleryURL. "', WEEKOFYEAR(NOW()), YEAR(NOW()), 'DIVE-CBR', now())";	
											
							if (!$mysqli->query($insertPicQry)) {
								$updteqry = " UPDATE pg_schedule_tracker_detail SET Error = '".$mysqli->error."', CronStatus = 'Error' 
									 WHERE CompetitorID = '".$compid."' ";
								$dbmysqli->query($updteqry);	
//------------MAIL FUNCTION---------------
								$objmail->mailfunction(array("subject" => 'Romaine CBR - Competitor Items Insert Error', "sendmail" => "No", "CC" => "CC: vaithi.d@apaengineering.com",
								"msg" => $mysqli->error));
								die('<P>Error thrown : ' .  $mysqli->error);
							} else {
								$mysqli->query($insertPicQry);
							}
						}

						$updateCompQry = "update competitor set LastRunFromDate = '" . $lastrunfromdate . "', LastRunToDate = '" . $lastruntodate . "', LastPageNo = " . $pageno . ", 
						TotalPage = " . $totpages . ", LastRunID = " . $lastrunid . ", errortype = NULL, recorddate = now() where compid = " . $compid;
							if (!$mysqli->query($updateCompQry)) {
//--------------------MAIL FUNCTION---------------		
								$objmail->mailfunction(array("subject" => 'Romaine CBR - Competitor Error', "sendmail" => "No", "CC" => "CC: vaithi.d@apaengineering.com", 
								"msg" => $mysqli->error));						
								die('<P>Error thrown : ' .  $mysqli->error);
							} else {
								$mysqli->query($updateCompQry);
							}
						//$mysqli->query($updateCompQry);
					}
					$lastpageno = $pageno - 1;
				}
				$runid++;
					
			}
			$runid = 0;
			
			$selqry = " SELECT SUM(a.total) as total FROM (SELECT SUM(TotalPage) AS 'total' FROM competitor_run_status WHERE DATE(RecordDate) = CURDATE() AND CompID = '".$compid."' AND TotalPage > 1 UNION 
						SELECT COUNT(1) AS 'total' FROM competitor_run_status WHERE DATE(RecordDate) = CURDATE() AND CompID = '".$compid."' ) a ";
			$resval = $mysqli->query($selqry);	
			$resvale = $resval->fetch_row();
			$totcalls = $resvale[0];
			
			$selqury = " SELECT COUNT(DISTINCT ItemID) as totalcnt FROM competitor_item_details WHERE CompID = '".$compid."' AND DATE(RecordDate) = CURDATE() ";
			$resvalue = $mysqli->query($selqury);	
			$resuvale = $resvalue->fetch_row();
			$totrecord = $resuvale[0];
			
			$endDate = date('Y-m-d H:i:s');
			$uptqry = " UPDATE pg_schedule_tracker_detail SET EndDate = '".$endDate."', TotalCalls = '".$totcalls."', RecordCount = '".$totrecord."', CronStatus = 'Success' 
						WHERE STDID = '".$autoid."' ";
			$dbmysqli->query($uptqry);
			
			$balncecomp++;
		
		}
		
		$endDate = date('Y-m-d H:i:s');
		$uptqry = " UPDATE pg_schedule_tracker_header SET EndDate = '".$endDate."' WHERE SchedulerID = '".$schdulerID."' ";
		$dbmysqli->query($uptqry);
		
	}

	$selctqry = "SELECT SchedulerID, CompetitorID, StartDate, EndDate, TotalCalls, RecordCount FROM pg_schedule_tracker_detail WHERE activeStatus = 1 and  SchedulerID = '".$schdulerID."' ";
	$resarr = $dbmysqli->query($selctqry);
	while($row = $resarr->fetch_assoc()) 
	{
		$resultset[] = $row;
	}
	if(count($resultset) > 0) {
		$msg = " Details: ". "<br/>".
		        " Account ID : Romaine". "<br/>".
				"Run Date : ". date('Y-m-d')."<br/>".
				"<html>
					<head>
						<title>Competitors Details:</title>
					</head>
					<body>
						<table border='1' cellpadding='5' cellspacing='0'>
						  <tr>
						    <th>Competitors</th>
						    <th>Start Date</th>
						    <th>End Date</th>
						    <th>Total calls</th>
						    <th>Total Records</th>
						  </tr>";
			foreach ($resultset as $resultvalue) {	
				$compname = $resultvalue['CompetitorID'];
				$StartDate = $resultvalue['StartDate'];
				$EndDate = $resultvalue['EndDate'];
				$TotalCalls = $resultvalue['TotalCalls'];
				$RecordCount = $resultvalue['RecordCount'];
					$msg .= "<tr>
						  <td>".$compname."</td>
						  <td>".$StartDate."</td>
						  <td>".$EndDate."</td>
						  <td>".$TotalCalls."</td>
						  <td>".$RecordCount."</td>
						</tr>";
			}
					$msg .= "</table>
					</body>
				</html>";
	}
		 $objmail->mailfunction(array("subject" => 'Successfully Completed For Romaine', "sendmail" => "Yes", "CC" => "CC: tamilselvan.s@apaengineering.com", "msg" => $msg));

		$spafterqry = "CALL pg_after_cron_run_process();";
		$mysqli->query($spafterqry);
		
		$trackqry = "CALL dive_api_run_track('Trend Update Process Started', 'Sales Trend Update', 0, 0, '', '', NOW(), WEEK(NOW()));";
		$mysqli->query($trackqry);
		$sptrendqry = "CALL dive_sales_trend_process();";
		$mysqli->query($sptrendqry);
		$trackqry = "CALL dive_api_run_track('Trend Update Process Ended', 'Sales Trend Update', 0, 0, '', '', NOW(), WEEK(NOW()));";
		$mysqli->query($trackqry);
	
	unset($mysqli);
	unset($dbmysqli);
	
	function errorapicallexceed() {
		if(isset($xml->Errors))
		{
			$errorsset = $xml->Errors->ErrorCode;	
			
			if ($errorsset == 518)
			{
				die('<P>Exceeded Daily Call Limit');
			}
			else
			{
				die('<P>Exceeded Daily Call Limit ' . $errorsset);
			}
		}
	}

?>
</table>