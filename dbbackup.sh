#!/bin/bash
# Simple script to backup MySQL databases

# Parent backup directory
backup_parent_dir="/var/backups/mysql"

# MySQL settings
mysql_user="apadive"
mysql_password="DiveApa2018*"


# Create backup directory and set permissions
backup_date=`date +%Y_%m_%d_%H_%M`
backup_dir="${backup_parent_dir}/${backup_date}"
echo "Backup directory: ${backup_dir}"
mkdir -p "${backup_dir}"
chmod 700 "${backup_dir}"


# Get MySQL databases
mysql_databases=`echo 'show databases' | mysql --user=${mysql_user} --password=${mysql_password} -B | sed /^Database$/d`

# Backup and compress each database
for database in $mysql_databases
do
  if test "${database}" = "information_schema" 
  then
        additional_mysqldump_params="--skip-lock-tables"
  elif test "${database}" = "performance_schema"
  then
        additional_mysqldump_params="--skip-lock-tables"
  else
        additional_mysqldump_params=""
  fi

  echo " "
  echo " "

  echo "Creating backup of \"${database}\" database"
  mysqldump ${additional_mysqldump_params} --user=${mysql_user} --password=${mysql_password} ${database} | gzip > "${backup_dir}/${database}_${backup_date}.gz"
  chmod 600 "${backup_dir}/${database}_${backup_date}.gz"
  /var/usr/gdrive/gdrive-linux-x64 upload "${backup_dir}/${database}_${backup_date}.gz" --parent 1x-sXKAACWCu7n_vJ1AoWagiES6DbdrUR
done



#bakdatabase="completetractor"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

#bakdatabase="arrowhead"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

#bakdatabase="romaine"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

#bakdatabase="directedheart"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

#bakdatabase="opticat"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

#bakdatabase="titanengine"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

#bakdatabase="pricesmart"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

#bakdatabase="reachcooling"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

#bakdatabase="GreenCheck"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

#bakdatabase="divedemo"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

#bakdatabase="arrowhead_staging"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

#bakdatabase="romaine_staging"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

#bakdatabase="fourgreenauto"
#echo "Creating backup of \"${bakdatabase}\" database"
#mysqldump --user=${mysql_user} --password=${mysql_password} ${bakdatabase} | gzip > "${backup_dir}/${bakdatabase}.gz"
#chmod 600 "${backup_dir}/${bakdatabase}.gz"

