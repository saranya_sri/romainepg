<?php
/*  © 2013 eBay Inc., All Rights Reserved */ 
/* Licensed under CDDL 1.0 -  http://opensource.org/licenses/cddl1.php */
    //show all errors - useful whilst developing
    error_reporting(E_ALL);

    // these keys can be obtained by registering at http://developer.ebay.com
    
    $production         = true;   // toggle to true if going against production
    $compatabilityLevel = 535;    // eBay API version
    
    if ($production) {
        $devID = '587f566f-a66b-4601-a226-382be102ec56';   // these prod keys are different from sandbox keys
        $appID = 'Nalmuthu-muthudiv-PRD-e08fef0ab-a6fcc7cc';
        $certID = 'PRD-08fef0abc40a-cce9-4f6c-9ca8-f42a';
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.ebay.com/ws/api.dll';      // server URL different for prod and sandbox
        //the token representing the eBay user to assign the call with
        $userToken = 'AgAAAA**AQAAAA**aAAAAA**o4kBWQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6ACkYqgDZOEqAmdj6x9nY+seQ**lbcDAA**AAMAAA**dxpzWP3eg+Ge4TJFF6ixGctrctsQ99ss+Ti0550whyu44cfq5rJTigFd4nIOa15YwNoWQdImrsu53MBxJJMhdwmPzyd++RtViphJzfvXaX1b2lJj0sonJnsqNUYUGfCJ8el19CslcxEXT1Sr1i17O70DGkBgldX/WS2YrbQtBoNm4jTJ6+NeKBtUqliwZkdTXaVqSls8IUkwqGTHz/PCEVEde6GM8RkiXwuaGP6gpLlIB4lrnmqjq/ftFrW+FwGGzBvH6I5ZCOu2gTQnLx1lM5xaWl/hv9J/xO5mh7UDe1PC6BXpJpVboVgb0+xMXCE1yEkQmMDARkrNBJ+998KdkUINbx+AxrWd661LpyCmIRJntR/y2Lnu8hzoyUWKA7gpWZVR1LxlT4aWLHXXRTKyr8Zt+uQsaq9gkwCbFYic74TYiO/oHx3kr16iTF7PDr1FGTe3aQHoAh5/wVOaHZuUvQhMX/n6jZLhBasXRzMxQOapR5L1MXTgbEVaquzu95SAirk/7L0WXas5tyTWTxTfR0WHM+EtruSTREZZv0zwQ6LYsZZs+xg1HOeNXIgRsjL1tGv3JjNV6Vicy1eF9K3CI0+5QbucwWjAAE11IgAjwcdWv0Xjb3jN1gkqsCVul1HC4rXLii5i2W1lNZWhGmAnWCc2px4tbD9BgqnaoPFL1YuX0AR9MQbGq9n+TO/2ltQ+nezegzcvdwNHlbyYbU8SlB/Dk38xrRS+t3fr2wtEEGtR0UlOhNhY6Yfmkcrue18o';          
    } else {  
        // sandbox (test) environment
        $devID = 'DDD_SAND';         // insert your devID for sandbox
        $appID = 'AAA_SAND';   // different from prod keys
        $certID = 'CCC_SAND';  // need three 'keys' and one token
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.sandbox.ebay.com/ws/api.dll';
        // the token representing the eBay user to assign the call with
        // this token is a long string - don't insert new lines - different from prod token
        $userToken = 'SANDBOX_TOKEN';                 
    }
    
    
?>