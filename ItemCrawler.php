<?php
	require_once('keysAnand.php');
	require_once('eBaySession.php');
	require_once('DBConnect.php');
	require_once('email.php');
	
	ob_implicit_flush(true);
	$mysqli = openDBconnect();
	$dbmysqli = DBconnect();
	$objmail = new objmail();


	$result = $resultItem = array();
	//$qry = " SELECT DISTINCT m.itemid, m.compid, '0' AS 'lastrunid' FROM competitor_item_details m WHERE m.compid = 74 AND weekno = 42 
	//			AND itemid NOT IN(SELECT itemid FROM `competitor_ibr_item_details` WHERE compid = 74) ";

	//$qry = " SELECT DISTINCT m.itemid, m.compid, c.lastrunid, c.inceptiondate  FROM competitor_master_list m, competitor c WHERE m.compid = c.compid AND c.Route = 'IBR' ";
	$qry = " SELECT DISTINCT m.itemid, m.compid, c.lastrunid, c.inceptiondate  FROM competitor_item_details m, competitor c WHERE m.compid = 74 	
				AND m.compid = c.compid AND m.weekno = WEEKOFYEAR(NOW()) AND m.listingstatus = 'Active'	  ";
	$res = $mysqli->query($qry);
	while($row = $res->fetch_assoc()) 
	{
		$result[] = $row;
	}
	//echo "<pre>"; print_r($result);
	//exit();
	$filter = 0;
	$br = "";
	//$categoryid = 178921;
 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<TITLE>Item Crawler</TITLE>
</HEAD>
<BODY>

<?php
 	$recordDate = $startDate = date('Y-m-d H:i:s');
	$accountid = 'PG006';

	$objmail->mailfunction(array("subject" => 'Romaine - DIVE IBR Data Processing Started', "sendmail" => "Yes", "CC" => "CC: saranya.m@apaengineering.com", "msg" => 'This is a automated mailer to intimate that the Romaine IBR Data processing program started succesfully.'));

	$insrtqry = " INSERT INTO pg_schedule_tracker_header (StartDate, AccountID, RecordDate) VALUES 
				('".$startDate."', '".$accountid."', '".$recordDate."') ";
	$dbmysqli->query($insrtqry);
	$last_id = $dbmysqli->insert_id;
	
	/* $spbefreqry = "CALL pg_before_cron_run_process();";
	//$mysqli->query($spbefreqry); */
	
	//echo "=====".$last_id; exit();
	
	$mailersub = 'Romaine - DIVE IBR Data Processing Started - Total - ' . count($result);
	
	$objmail->mailfunction(array("subject" => $mailersub, "sendmail" => "No", "CC" => "CC: saranya.m@apaengineering.com", "msg" => 'This is a automated mailer to intimate that the Romaine IBR Data processing program started succesfully.'));
		
		$schdulerID = $last_id;  
			
			$insqry = " INSERT INTO pg_schedule_tracker_detail (SchedulerID, AccountID, StartDate, CronStatus, RecordDate) VALUES 
			          ('".$schdulerID."', '".$accountid."', '".$startDate."', 'InProgress', '".$recordDate."') ";
			$dbmysqli->query($insqry);	  
			$autoid = $dbmysqli->insert_id;		
			
	$runid = 0;
	if(count($result) > 0)
	{
		foreach($result as $reset)
		{
			$lastrunid = 0;
			$compid = $reset['compid'];
			$itemid = $reset['itemid'];
			$lastrunid = $reset['lastrunid'];
		//	$inceptiondate = $reset['inceptiondate'];
			
			echo "\r\n";
			echo "Processing Item ID : " . $itemid;
			echo "\r\n";
			echo "\r\n";
			echo "Processing Comp ID : " . $compid;
			echo "\r\n";
			

			$format = 'Y-m-d H:i:s';
			$lasttodate = DateTime::createFromFormat($format, '1980-01-01 01:01:01');
			$lastdate = date_format($lasttodate, 'Y-m-d');
			$runonce = 1; 
		
			
				$lastrunid = $lastrunid + 1;
				$runonce = 1;

				echo "\r\n";

				$siteID = 0;
				$verb = 'GetItem';

				///Build the request Xml string
				$requestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
				$requestXmlBody .= '<GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
				$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$userToken</eBayAuthToken></RequesterCredentials>";
				$requestXmlBody .= "<IncludeItemSpecifics>true</IncludeItemSpecifics>";
				$requestXmlBody .= "<ItemID>$itemid</ItemID>";
				$requestXmlBody .= "<IncludeItemCompatibilityList>true</IncludeItemCompatibilityList>";
				$requestXmlBody .= "<IncludeTaxTable>true</IncludeTaxTable>";
				$requestXmlBody .= "<DetailLevel>ReturnAll</DetailLevel>";
				$requestXmlBody .= '</GetItemRequest>';
				
				//echo "<pre>"; print_r($requestXmlBody);
				//exit;
				
				//Create a new eBay session with all details pulled in from included keys.php
				$session = new eBaySession($userToken, $devID, $appID, $certID, $serverUrl, $compatabilityLevel, $siteID, $verb);

				//send the request and get response
				$responseXml = $session->sendHttpRequest($requestXmlBody);
				
				if(isset($responseXml->Errors))
						{
							$errorsset = $xml->Errors->ErrorCode;	
							
							if ($errorsset == 518)
							{
								die('<P>Exceeded Daily Call Limit');
							}
							else
							{
								die('<P>Exceeded Daily Call Limit ' . $errorsset);
							}
						}
						

				if(isset($responseXml->Errors))
				{
					//print_r($responseXml);
					$errorsset = $xml->Errors->ErrorCode;
					if ($errorsset == 518)
					{
						$updateCompQry = "update competitor set errortype = '" . $responseXml . "', recorddate = now() where compid = " . $compid;
						if (!$mysqli->query($updateCompQry)) {
	//--------------------MAIL FUNCTION---------------			
								$objmail->mailfunction(array("subject" => 'exceeded usage limit', "sendmail" => "No", "CC" => "CC: saranya.m@apaengineering.com", "msg" => $responseXml));							
							die('<P>Error thrown : ' .  $mysqli->error);
						} else {
							$mysqli->query($updateCompQry);
						}
						die('<P>Exceeded Daily Call Limit');
					}
					else
					{
						die('<P>Exceeded Daily Call Limit ' . $errorsset);
					}
				}
					
				//Xml string is parsed and creates a DOM Document object
				$responseDoc = new DomDocument();
				$responseDoc->loadXML($responseXml);
				
				$xml = simplexml_load_string($responseXml);
				// print_r($xml);
				
				/* if (is_null($inceptiondate)) {
					if(isset($xml->Item->Seller)) {
						$inceptiondate = $xml->Item->Seller->RegistrationDate;
						$sellerStatus = $xml->Item->Seller->Status;
						$sellerSite = $xml->Item->Seller->Site;
						$sellerLevel = $xml->Item->Seller->SellerInfo->SellerLevel;
						
						$inceptiondate = str_replace("T"," ",$inceptiondate);
						$inceptiondate = str_replace(".000Z","",$inceptiondate);
					
						$updQry = "update competitor set InceptionDate = '" . $inceptiondate . "', CompetitorStatus = '".$sellerStatus."', CompetitorSite = '".$sellerSite."',
									  Competitorlevel = '".$sellerLevel."' where CompID = '" . $compid . "'";

						if (!$mysqli->query($updQry)) {
							die('<P>Error thrown : ' .  $mysqli->error);
						} else {
							$mysqli->query($updQry);
						}
					}
				} */
				
				unset($session);
				
				foreach($xml->Item as $resultval) 
				{
					$listingdate = $productid = '';
					$ItemCompCount = 0;
					$startdate = $resultval->ListingDetails->StartTime;
					$datetm = str_replace("T"," ",$startdate);
					$datetm = str_replace(".000Z","",$datetm);
					if($datetm != '') {
						$listedon = DateTime::createFromFormat('Y-m-d H:i:s',$datetm);
						$listingdate = $listedon->format('Y-m-d H:i:s');
			
					$listingyear = $listedon->format('Y');
					$listingweek = $listedon->format('W');
					}

					$itemID = $resultval->ItemID;
					$sku = $resultval->SKU;
					$title = $resultval->Title;
					$category = $resultval->PrimaryCategory->CategoryName;
					$categoryID = $resultval->PrimaryCategory->CategoryID;
					$quantity = $resultval->Quantity;
					$currprice = $resultval->SellingStatus->CurrentPrice;
					$qtysold = $resultval->SellingStatus->QuantitySold;
					$ListingStatus = $resultval->SellingStatus->ListingStatus;
					//$ViewItemURL = $resultval->ListingDetails->ViewItemURL;
					$GalleryURL = $resultval->PictureDetails->GalleryURL;
					$ViewItemURL = 'http://www.ebay.com/itm/'.$itemID;
					
					$productid = $resultval->ProductListingDetails->ProductReferenceID;
					$ItemCompCount = $resultval->ItemCompatibilityCount;
					
					$shipping = 0; $servicecost = 0; $ConditionID = 1000; $ConditionDisplayName = 'New';
					$itemcountry = $resultval->Country;
					$itemLocation = $resultval->Location;
					$ConditionID = $resultval->ConditionID;
					$ConditionDisplayName = $resultval->ConditionDisplayName;
					$ShippingService = $resultval->ShippingDetails->ShippingServiceOptions;
					
					if (count($ShippingService) > 0) {
						$servicecost  = $ShippingService->ShippingServiceCost;
						$shipping = $ShippingService->FreeShipping;
					} else {
						$servicecost = $ShippingService->ShippingServiceCost;
					}
					
					if ($servicecost > 0) {
						$shipping = 'false';
					} else {
						$shipping = 'True';
					}
					
					$variationcount = 0;

					$variationcount = count($resultval->Variations->Variation);
					
					$NameValueList = $resultval->ItemSpecifics;
					$brand = $mpn = $ipn = $opn = ''; 
					$namelist = $NameValueList->NameValueList;
					if(count($namelist) > 0) {
						foreach ($namelist as $reslt) {
							$name = $reslt->Name;
							$Value = $reslt->Value;
							if($reslt->Name == 'Brand') {
								$brand = $reslt->Value;
							}
							if($reslt->Name == 'Manufacturer Part Number' || $reslt->Name == 'MPN' || $reslt->Name == 'mpn') {
								$mpn = $reslt->Value;
							}
							if($reslt->Name == 'Interchange Part Number' || $reslt->Name == 'IPN' || $reslt->Name == 'ipn') {
								$ipn = $reslt->Value;
							}
							if($reslt->Name == 'Other Part Number' || $reslt->Name == 'OPN' || $reslt->Name == 'opn') {
								$opn = $reslt->Value;
							}
						}
					}

					$TopRatedListing = '';
					$sku = str_replace("'", "\'" ,$sku);
					$sku = str_replace('"', '\"' ,$sku);
				
					$category = str_replace("'", "\'" ,$category);
					$category = str_replace('"', '\"' ,$category);
					
					$itemLocation = str_replace("'", "\'" ,$itemLocation);
					$itemLocation = str_replace('"', '\"' ,$itemLocation);
					
					$titleupdated = str_replace("'", "\'" ,$title);
					$titleupdated = str_replace('"', '\"' ,$titleupdated);
					if($itemID != '') {
						$insertQry = "INSERT into competitor_IBR_item_details(CompID, ItemID, SKU, Title, Category, CategoryID, Quantity, CurrentPrice, QuantitySold, ListingStatus, ItemURL, ListingDate, 
						RunID, ProductID, MfrPartNo, BrandName, IPN, OPN, CompatibilityCount, RecordDate, WeekNo, YearNo, Country, Location, ConditionID, ConditionName, ShippingCost, 
						FreeShipping, VariantCount, TopRatedListing)
						values(" . $compid . ", '" . $itemID . "', '" . $sku . "', '" . $titleupdated . "', '" . $category . "', '" .  $categoryID . "', '" .$quantity . "', '" . $currprice . "', 
						'" . $qtysold . "', '" . $ListingStatus . "', '" . $ViewItemURL. "', '" . $listingdate . "', '" . $lastrunid . "', '".$productid."', '" . $mpn . "', '" . $brand . "', 
						'".$ipn."', '".$opn."', '".$ItemCompCount."', now(), 
						WEEKOFYEAR(NOW()), YEAR(NOW()), '".$itemcountry."', 
						'".$itemLocation."', '".$ConditionID."', '".$ConditionDisplayName."', '".$servicecost."', '".$shipping."'," . $variationcount . " , '" . $TopRatedListing . "' )";
						
						if (!$mysqli->query($insertQry)) {
							echo $insertQry;
							$updteqry = " UPDATE competitor SET errortype = '".$mysqli->error."'
										 WHERE CompID = '".$compid."' ";
							$dbmysqli->query($updteqry);	
	//------------MAIL FUNCTION---------------
							$objmail->mailfunction(array("subject" => 'Competitor Items Insert Error', "sendmail" => "No", "CC" => "CC: saranya.m@apaengineering.com", "msg" => $mysqli->error));
							die('<P>Error thrown : ' .  $mysqli->error);
						} else {
							$mysqli->query($insertQry);
						}
					}
					//$mysqli->query($insertQry);
							/* $insertPicQry = "INSERT into competitor_item_picture_list(CompID, ItemID, RunID, PictureID, PictureURL, WeekNo, YearNo, Created_By, Created_On) 
											values(" .$compid. ", '" .$itemID. "', '".$lastrunid. "', 0, '" .$GalleryURL. "', WEEK(NOW()), YEAR(NOW()), 'DIVE-IBR', now())";	
											
							if (!$mysqli->query($insertPicQry)) {
								$updteqry = " UPDATE competitor SET errortype = '".$mysqli->error."'
									 WHERE CompID = '".$compid."' ";
								$dbmysqli->query($updteqry);	
//------------MAIL FUNCTION---------------
								$objmail->mailfunction(array("subject" => 'Competitor Items Insert Error', "sendmail" => "No", "CC" => "CC: saranya.m@apaengineering.com", "msg" => $mysqli->error));
								die('<P>Error thrown : ' .  $mysqli->error);
							} else {
								$mysqli->query($insertPicQry);
							} */
				}

				$updateCompQry = "update competitor set LastRunID = " . $lastrunid . ", errortype = NULL, recorddate = now() where compid = " . $compid;
				if (!$mysqli->query($updateCompQry)) {
//------------MAIL FUNCTION---------------	
					$objmail->mailfunction(array("subject" => 'Competitor Error', "sendmail" => "No", "msg" => $mysqli->error));				
					die('<P>Error thrown : ' .  $mysqli->error);
				} else {
					$mysqli->query($updateCompQry);
				}
				//$mysqli->query($updateCompQry);

				
				$runid++;

	
		}

		$totcalls = count($result);
		
		$selqury = " SELECT COUNT(DISTINCT ItemID) as totalcnt FROM competitor_IBR_item_details WHERE DATE(RecordDate) = CURDATE() ";
		$resvalue = $mysqli->query($selqury);	
		$resuvale = $resvalue->fetch_row();
		$totrecord = $resuvale[0];
		
		$endDate = date('Y-m-d H:i:s');
		$uptqry = " UPDATE pg_schedule_tracker_detail SET EndDate = '".$endDate."', TotalCalls = '".$totcalls."', RecordCount = '".$totrecord."', CronStatus = 'Success' 
		            WHERE STDID = '".$autoid."' ";
		$dbmysqli->query($uptqry);
		
		
		$endDate = date('Y-m-d H:i:s');
		$uptqry = " UPDATE pg_schedule_tracker_header SET EndDate = '".$endDate."' WHERE SchedulerID = '".$schdulerID."' ";
		$dbmysqli->query($uptqry);
		$runid = 0;
	}

	$selctqry = "SELECT SchedulerID,  StartDate, EndDate, TotalCalls, RecordCount FROM pg_schedule_tracker_detail WHERE activeStatus = 1 and  SchedulerID = '".$schdulerID."' ";
	$resarr = $dbmysqli->query($selctqry);
	
	
	while($row = $resarr->fetch_assoc()) 
	{
		$resultset[] = $row;
	}
	if(count($resultset) > 0) {
		$msg = " Details: ". "<br/>".
		        " Account ID : Romaine". "<br/>".
				"Run Date : ". date('Y-m-d')."<br/>".
				"<html>
					<head>
						<title>Item Run Details:</title>
					</head>
					<body>
						<table border='1' cellpadding='5' cellspacing='0'>
						  <tr>
						    <th>Start Date</th>
						    <th>End Date</th>
						    <th>Total calls</th>
						    <th>Total Records</th>
						  </tr>";
			foreach ($resultset as $resultvalue) {	
				$StartDate = $resultvalue['StartDate'];
				$EndDate = $resultvalue['EndDate'];
				$TotalCalls = $resultvalue['TotalCalls'];
				$RecordCount = $resultvalue['RecordCount'];
					$msg .= "<tr>
						  <td>".$StartDate."</td>
						  <td>".$EndDate."</td>
						  <td>".$TotalCalls."</td>
						  <td>".$RecordCount."</td>
						</tr>";
			}
					$msg .= "</table>
					</body>
				</html>";
	}
		 $objmail->mailfunction(array("subject" => 'Successfully Completed For Romaine', "sendmail" => "Yes", "CC" => "CC: saranya.m@apaengineering.com", "msg" => $msg));

		$spafterqry = "CALL pg_IBR_after_cron_run_process();";
		$mysqli->query($spafterqry);
	
	

	unset($mysqli);
	unset($dbmysqli);
?>
</table>