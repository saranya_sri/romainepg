<?php
/*  © 2013 eBay Inc., All Rights Reserved */ 
/* Licensed under CDDL 1.0 -  http://opensource.org/licenses/cddl1.php */
    //show all errors - useful whilst developing
    error_reporting(E_ALL);

    // these keys can be obtained by registering at http://developer.ebay.com
    
    $production         = true;   // toggle to true if going against production
    $compatabilityLevel = 535;    // eBay API version
    
    if ($production) {
        $devID = 'f01ca06b-e9cc-4526-bc8e-00b2d627b449';   // these prod keys are different from sandbox keys
        $appID = 'kalaisel-kalaisel-PRD-b66850dff-69f4dead';
        $certID = 'PRD-66850dff44fb-d5c7-48aa-8284-7671';
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.ebay.com/ws/api.dll';      // server URL different for prod and sandbox
        //the token representing the eBay user to assign the call with
        $userToken = 'AgAAAA**AQAAAA**aAAAAA**27UgWw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6ADmYqjDZSDpwidj6x9nY+seQ**mGIEAA**AAMAAA**6tCKsKTx5ulq1tUCXQEWzpA888AwJSP0BZYuJBSw7wxO+w/ZUBphFYD/5nRaGkYo5a7NrwUqzL8VZUiTwYr3WoYFwmhiaq85lSuNc1hGvGDkmxRvORshMqbVeaFuel/1FPudUWnyt1U7PVn6aAzDd/puAsUZh6Wuw9A/YPdyiO01hxla/HMHInM+4ML3Q9k49YzEYgspeJC17bMWXtRlvEZTQQuG+7cKV5BzRQtEfSeJNlnDF1gSp5o30vJr3GO1E9Qsz8r2U8M7rNmBy5+RerB5tUgGfFxBOELs+r8kraF0F/y5yQk0XloD4w3Xr+UVnoQ5zx9N1sGXe+EP2x1Gb7v20pNOG4eHkSIvS9Toz0ZKoSnuuHYCLU6NXiW+O9BrPLL89jW6EFwCIT83RDTDyO3LbFFeSiqjwmlg0Uq/lt4whsFgwK6v6w+2OULJwk0uMSd0OaSC6RPbi/gobQpcNIy+hw7+nkL8nODnVWtPnfal3OoDILK/Gf4aAiYb8gRz6iTsH118V9+EOt+8I6OJoJEhls6+oAIFM4sTScbH8PGKFzgoZkud1t4r8BNpcVanM75/A8xnfi5VbAKYIegrTTqJ4Q0LOdvGETS5hE0WtCvVX+AF1TBtiAiNYYhTG6txgP7xrxnjCnbVbgquM7jIk5yje2DAVwnNaruBmUqDkkZeW7a45/Qu5oKQzFC2R9bvfipYhoIuBUWQEJfKuetqxlvPI6g/rwYjWg6DOckVkgGJOYeXDYNqHDOJK4JQ8BBI';          
    } else {  
        // sandbox (test) environment
        $devID = 'DDD_SAND';         // insert your devID for sandbox
        $appID = 'AAA_SAND';   // different from prod keys
        $certID = 'CCC_SAND';  // need three 'keys' and one token
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.sandbox.ebay.com/ws/api.dll';
        // the token representing the eBay user to assign the call with
        // this token is a long string - don't insert new lines - different from prod token
        $userToken = 'SANDBOX_TOKEN';                 
    }
 
?>