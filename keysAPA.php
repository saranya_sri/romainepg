<?php
/*  © 2013 eBay Inc., All Rights Reserved */ 
/* Licensed under CDDL 1.0 -  http://opensource.org/licenses/cddl1.php */
    //show all errors - useful whilst developing
    error_reporting(E_ALL);

    // these keys can be obtained by registering at http://developer.ebay.com
    
    $production         = true;   // toggle to true if going against production
    $compatabilityLevel = 535;    // eBay API version
    
    if ($production) {
        $devID = '098ea620-f6a0-4a98-9d26-855a804c572d';   // these prod keys are different from sandbox keys
        $appID = 'APAEngin-AutoInve-PRD-5bffbb2e5-df0267de';
        $certID = 'PRD-bffbb2e55907-d466-4dbd-915f-694c';
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.ebay.com/ws/api.dll';      // server URL different for prod and sandbox
        //the token representing the eBay user to assign the call with
        $userToken = 'AgAAAA**AQAAAA**aAAAAA**V07gWg**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AGkoKgCZmHpQqdj6x9nY+seQ**P3gDAA**AAMAAA**5Ta9r5i/4rGpdz1lqBnYKqBHzV06l/SHQAyU3/85VwzyHBjMRUgBFS8fVOKzkqBNA//EPUKKC/xD5L2/BvX4Em7Xl4UIOA3UaAbKkerD+hguV5a65iB5c+IzUBbSuRPJfPEXlGmh0nkex5zCG+ixnWeeBxN6EEx3oHk0PVYZ2WnmJjwNgJuXlsHA36E+oxGJUMxr8nhKJRI7XsJiJoEj7p9G2Z5zEss142UhKwG5XcSnfB33k9in1fHZRtnBWPhqrDK6wFTcGuS1YB88xJOHAap8/6W9ZdAvPQza9SEK6Cvkj60gmeZWBq4uPw2vz1sE7uQd+HCB1K911Wyde3dCCpen3oOz1BalFbqKUvZs4C7xU41HTfNaN0DvVnqTc2KpNhRKGyr9KRYGJu4Syq1gkPhDofpNXBVzBDSh24Gc3Iipc6FEx8Zso8ZYmRZTtFvsoQfwX+z1SRhGsQAsU2bYoDSh/m1BvMGzWs+iTmsqRi0yA5NYO2bjpGQU+E0K4YStPL0ARvlNxCso2tDTUg9ekw/U/Gr/ZAK2ruYxJ1nXKTFKmpAib2eBOKetK/dIhrmANlhaePWbmGp8V6pOrKU1y0f+owHnC6kSt3q8PIhVhZ4GJy7qRvn569Zwtirvrk0bwwow6ZgwduovViiyqXTxE7frpS1kjrWi4fJDpFXqZYu0XfXiuHeWqcMeLATeBamAGKgv3ZaAiwPGweN0oNrHaY/hk5jEDDf1bXN4m670iqVsSn0ZAWMVt4xqHoF7KH15';          
    } else {  
        // sandbox (test) environment
        $devID = 'DDD_SAND';         // insert your devID for sandbox
        $appID = 'AAA_SAND';   // different from prod keys
        $certID = 'CCC_SAND';  // need three 'keys' and one token
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.sandbox.ebay.com/ws/api.dll';
        // the token representing the eBay user to assign the call with
        // this token is a long string - don't insert new lines - different from prod token
        $userToken = 'SANDBOX_TOKEN';                 
    }
    
    
?>