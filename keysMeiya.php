<?php
/*  © 2013 eBay Inc., All Rights Reserved */ 
/* Licensed under CDDL 1.0 -  http://opensource.org/licenses/cddl1.php */
    //show all errors - useful whilst developing
    error_reporting(E_ALL);

    // these keys can be obtained by registering at http://developer.ebay.com
    
    $production         = true;   // toggle to true if going against production
    $compatabilityLevel = 535;    // eBay API version
    
    if ($production) {
        $devID = 'b5b15ad7-8bad-49d2-aac7-446711c51f55';   // these prod keys are different from sandbox keys
        $appID = 'meiyaj-meiyaapa-PRD-92ccbdebc-74670a60';
        $certID = 'PRD-2ccbdebcaf35-8968-4195-98eb-7a1c';
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.ebay.com/ws/api.dll';      // server URL different for prod and sandbox
        //the token representing the eBay user to assign the call with
        $userToken = 'AgAAAA**AQAAAA**aAAAAA**sbEgWw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6ADmYqjDZCCoQ2dj6x9nY+seQ**lGIEAA**AAMAAA**Sa+Q1hy44L668Y0/JSjs9StfAIMh/KiH6DY/VCJuXb2YP2N2sldp29N+o2WIMoUyKPYqbc7i2PzkM51yk7Y1Km/iOLlyimP/H/O9i1/EoUC+45r1F6sfHEj05hm602yXbsbacK5fjnx2bkfCdAj9q+T5uTL5/5jnRiM7KGMQSBKWLQGUvsAyj1s9mvHpbPDly0gjf7isvr2th0wSGqk8YUyai0xo81QOhovWjuTxvAAJRdU62Bgdbs6yI9wdYskn9StUp03t0eHZSwtx7ok/NJRedc5uB4IOEAmr5IlbxrRb0oOmKr6lc6Qr4D5qxubzsRp+sR48ueJVu6u3/6JJX/Y+lS5u1Q8jPK1eJxBPzPRoPtG2j2FEAlV3MX52VQIyqTepleHT25cJmKqb9rod9BV12UJTlTrQPoV2Ar88VbYTRj/b0zYMgpbVUVQORlXxRGE1EAnfUBByeD8/xvgCBZpwvPIn7ZF3eiQmccncfAr/DtdpeLxAz+sCtcDJMkps9acWbMy4ZUtJZjgiBTIG/I8CULeHfbXiQgsWVpguitkA/K480PiZhNdIvGPpYgGBXS6YNf3j3lcwMGco/JteM7LfFtEe/4ACGZr0w46iHTY663QbFOZgdXRW0tJl9OH352u0PCW8O9lJ+XPsTiFqbDqOJ3Y44vH0Oayvprs8QBTh6Rx+KbNLJzMLtF7rESKpKSyd6qIEj7ECqIvPHInSBXcJCQgaroEXntbPysMpqyT8sHCqhHZ3yRll5XN4vNB8';          
    } else {  
        // sandbox (test) environment
        $devID = 'DDD_SAND';         // insert your devID for sandbox
        $appID = 'AAA_SAND';   // different from prod keys
        $certID = 'CCC_SAND';  // need three 'keys' and one token
        //set the Server to use (Sandbox or Production)
        $serverUrl = 'https://api.sandbox.ebay.com/ws/api.dll';
        // the token representing the eBay user to assign the call with
        // this token is a long string - don't insert new lines - different from prod token
        $userToken = 'SANDBOX_TOKEN';                 
    }
 
?>